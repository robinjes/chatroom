const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const isProd = process.argv.indexOf('-p') !== -1;
const nodeEnv = isProd ? 'production' : 'development';

const sourcePath = path.join(__dirname, 'src/');
const publicPath = path.join(__dirname, 'target/');

const extractCSS = new ExtractTextPlugin({ filename: 'styles/[name].bundle.css', disable: false, allChunks: true });

const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'scripts/vendor.bundle.js'
  }),
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
  }),
  new HtmlWebpackPlugin({
    template: sourcePath + '/template/main.template.html',
    production: isProd,
    inject: true,
  }),
];

const jsEntry = [
  sourcePath + 'index'
];

if (isProd) {
  plugins.push(
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    extractCSS
  );
} else {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  );
}

module.exports = {
  devtool: isProd ? 'source-map' : 'cheap-module-source-map',
  context: sourcePath,
  entry: {
    js: jsEntry,
    vendor: [
      'react',
      'react-dom',
      'redux',
      'react-redux',
      'redux-thunk',
      'redux-persist'
    ]
  },
  target: 'web',
  output: {
    path: publicPath,
    filename: 'scripts/[name].[chunkhash].js',
    chunkFilename: '[chunkhash].js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        // ESLint
        test: /\.(js|jsx)$/,
        enforce: 'pre',
        use: {
          loader: 'eslint-loader',
          options: {
            configFile: '.eslintrc',
            include: sourcePath,
            exclude: /node_modules/,
            quiet: true // to allow 'FriendlyErrorsWebpackPlugin' to work
          }
        }
      },
      {
        test: /\.css$/,
        use: isProd ?
          extractCSS.extract({
            fallback: 'style-loader',
            use: [
              { loader: 'css-loader', options: { importLoaders: 1, sourceMap: true } },
              'postcss-loader'
            ]
          }) :
          [
            { loader: 'style-loader' },
            { loader: 'css-loader',
              options: {
                sourceMap: true,
              },
            },
            { loader: 'postcss-loader' }
          ]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      }
    ],
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx'],
    modules: [
      sourcePath,
      'node_modules'
    ],
    alias: {
      src: sourcePath,
      javascripts: 'src/javascripts',
      styles: 'src/styles',
      components: 'javascripts/components',
      actions: 'javascripts/actions',
      reducers: 'javascripts/reducers',
      store: 'javascripts/store',
      utils: 'javascripts/utils'
    }
  },
  plugins
};
