module.exports = {
  plugins: {
    // In order to use import into css file, doesn't use webpack alias :/
    'postcss-import': {
      path: ['./src/']
    },
    // In order to use CSS 4.0
    'postcss-cssnext': {
      browsers: ['last 4 versions', '> 5%'],
    }
  }
};