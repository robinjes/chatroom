import createReducer from 'utils/createReducer';

/* Initial state */
let now = new Date();
const initialState = [
    {
      userid: '11111-11111',
      time: now.getTime(),
      timeFrmt: now.toUTCString(),
      text: 'Bonjour, puis-je vous aider ?'
    }
  ],
  /* Message reducer */
  messages = {
    ADD_MESSAGE: (state = [], action) => {
      const userid = action.payload.userid;
      const text = action.payload.text;
      if (userid && text && text !== '') {
        now = new Date();
        const time = now.getTime();
        const timeFrmt = now.toUTCString();
        return [
          ...state,
          { userid, time, timeFrmt, text }
        ];
      }
      return state;
    }
  };

export default createReducer(initialState, messages);
