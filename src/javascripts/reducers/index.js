import { combineReducers } from 'redux';
import users from './users';
import messages from './messages';

const app = combineReducers({
  users,
  messages
});
export default app;
