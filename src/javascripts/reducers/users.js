import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = [
    {
      id: '11111-11111',
      username: 'Caroline K.'
    },
    {
      id: '22222-22222',
      username: 'Bernard D.'
    }
  ],
  /* Users reducer */
  users = {};

export default createReducer(initialState, users);
