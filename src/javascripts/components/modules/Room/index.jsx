// Librairies
import { connect } from 'react-redux';
// Components
import Room from './Room';

// Connect
const mapStateToProps = state => ({
    users: state.users
  }),
  RoomConnector = connect(
    mapStateToProps
  )(Room);

export default RoomConnector;
