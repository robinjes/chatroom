// Librairies
import React, { Component } from 'react';
// Styles
import './style.css';

// Render
const Header = () => (
  <header className="header">
    <h1>Chatroom</h1>
  </header>
);

export default Header;
