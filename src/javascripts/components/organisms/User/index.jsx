// Librairies
import { connect } from 'react-redux';
// Actions
import addMessage from 'actions';
// Components
import UserCtn from './UserCtn';

// Connect
const mapDispatchToProps = dispatch => ({
    addMessage: (userid, message) => (
      dispatch(addMessage(userid, message))
    )
  }),
  UserConnector = connect(
    null,
    mapDispatchToProps
  )(UserCtn);

export default UserConnector;
