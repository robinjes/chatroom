// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import ChatPanel from 'components/molecules/ChatPanel';
import ChatCtrl from 'components/molecules/ChatCtrl';
// Styles
import './styles.css';

// Render
const User = ({ profile, currentMessage, sendFn, typeMessageFn }) => (
  <div className="user">
    <h2>{profile.username}</h2>
    <ChatPanel userid={profile.id} />
    <ChatCtrl
      currentMessage={currentMessage}
      sendFn={sendFn}
      typeMessageFn={typeMessageFn}
    />
  </div>
);

User.propTypes = {
  profile: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])).isRequired,
  currentMessage: PropTypes.string,
  typeMessageFn: PropTypes.func.isRequired,
  sendFn: PropTypes.func.isRequired
};

User.defaultProps = {
  currentMessage: ''
};

export default User;
