// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import User from './User';

// Render
class UserCtn extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentMessage: ''
    };

    this.typeMessage = this.typeMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
  }

  typeMessage(evt) {
    this.setState({ currentMessage: evt.target.value });
  }

  sendMessage(evt) {
    if ((evt.charCode === 13 && !evt.shiftKey) || evt.type === 'click') {
      this.props.addMessage(this.props.profile.id, this.state.currentMessage);
      this.setState({ currentMessage: '' });
    }
  }

  render() {
    const { profile } = this.props;
    return (
      <User
        profile={profile}
        currentMessage={this.state.currentMessage}
        typeMessageFn={this.typeMessage}
        sendFn={this.sendMessage}
      />
    );
  }
}

UserCtn.propTypes = {
  profile: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ])).isRequired,
  addMessage: PropTypes.func.isRequired
};

export default UserCtn;
