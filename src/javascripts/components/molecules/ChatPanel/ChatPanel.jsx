// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Styles
import './styles.css';

// Render
class ChatPanel extends Component {

  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    this.chatpanel.scrollTop = this.chatpanel.scrollHeight;
  }

  render() {
    const { messages, userid } = this.props;
    return (
      <div className="chatPanel" ref={(div) => { this.chatpanel = div; }}>
        {messages.map(message => (
          <div key={message.time} className={`chatPanel-message ${message.userid === userid ? 'message-sender' : 'message-receiver'}`}>
            <div className="chatPanel-message__content">{message.text}</div>
            <span className="chatPanel-message__time"><br />{message.timeFrmt}</span>
          </div>
        ))}
      </div>
    );
  }
}

ChatPanel.propTypes = {
  userid: PropTypes.string.isRequired,
  messages: PropTypes.arrayOf(PropTypes.object)
};

ChatPanel.defaultProps = {
  messages: []
};


export default ChatPanel;
