// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import Field from 'components/atoms/Field';
import Button from 'components/atoms/Button';
// Styles
import './styles.css';

// Render
const ChatCtrl = ({ currentMessage, sendFn, typeMessageFn }) => (
  <div className="chatCtrl">
    <Field
      id="challenger-user__name"
      className="messagefield"
      value={currentMessage}
      placeholder="Message"
      onKeyPress={sendFn}
      onChange={typeMessageFn}
    />
    <Button className="messageBtn" onClick={sendFn}>
      {/* eslint-disable max-len */}
      <svg className="svg-icon" viewBox="0 0 20 20">
        <path d="M17.218,2.268L2.477,8.388C2.13,8.535,2.164,9.05,2.542,9.134L9.33,10.67l1.535,6.787c0.083,0.377,0.602,0.415,0.745,0.065l6.123-14.74C17.866,2.46,17.539,2.134,17.218,2.268 M3.92,8.641l11.772-4.89L9.535,9.909L3.92,8.641z M11.358,16.078l-1.268-5.613l6.157-6.157L11.358,16.078z" />
      </svg>
      {/* eslint-enable max-len */}
    </Button>
  </div>
);

ChatCtrl.propTypes = {
  currentMessage: PropTypes.string.isRequired,
  typeMessageFn: PropTypes.func.isRequired,
  sendFn: PropTypes.func.isRequired
};

export default ChatCtrl;
