// Librairies
import React, { Component } from 'react';
// Components
import Header from 'javascripts/components/modules/Header';
import Room from 'javascripts/components/modules/Room';
// Styles
import './style.css';

// Render
const App = () => (
  [
    <Header key="header" />,
    <div key="main" className="main">
      <Room />
    </div>
  ]
);

export default App;
