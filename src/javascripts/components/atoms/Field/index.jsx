// Librairies
import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './style.css';

const Field = props => (
  <div className={`${props.className ? props.className + ' ' : ''} field`}>
    {/* eslint-disable jsx-a11y/label-has-for */}
    {props.label !== '' &&
      <label htmlFor={props.id}>{props.label} :</label>
    }
    {/* eslint-enable jsx-a11y/label-has-for */}
    <input
      id={props.id}
      name={props.id}
      type={props.type}
      placeholder={props.placeholder}
      value={props.id}
      {...props}
    />
  </div>
);

Field.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string
};

Field.defaultProps = {
  className: '',
  type: 'text',
  label: '',
  value: '',
  placeholder: ''
};

export default Field;
