/* global __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ */
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import thunk from 'redux-thunk';
/* Reducers */
import combinedReducer from 'reducers';
/* Initial state */
import initialState from './initialState';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(combinedReducer, initialState, composeEnhancers(
  applyMiddleware(thunk),
  autoRehydrate()
));

persistStore(store);

export default store;
