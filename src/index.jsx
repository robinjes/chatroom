// Librairies
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
// Components
import AppCtn from 'components/environments/App';
// Store
import store from 'store';
// Styles
import 'styles/common.css';

// Render
ReactDOM.render((
  <Provider store={store}>
    <AppCtn />
  </Provider>
), document.getElementById('container'));
