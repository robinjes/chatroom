# Chatroom

Chatroom Front-end application.

Online demo : [http://dev2.lerobin.com/](http://dev2.lerobin.com/)

# Install
`npm install`

# Run
**Development mode** ([http://localhost:8080](http://localhost:8080)) :
`npm run watch`

**Production build** :
`npm run build`

# Tools/Versions

## Front-side
- **ES6**
- **React** as UI framework.
- **Redux** as state manager.
- **Redux Thunk Middleware** as Redux asynchronous actions middleware.
- **Redux Persist** as persisting & rehydrating Redux store tool.

## Development
- **Babel and plugins** as transpiler.
- **Babel CLI** as command line compiler.
- **Webpack** as modules bundler.
- **PostCSSNext** as next generation CSS transpiler.
- **Eslint and AirBnb config** as code checker.

# Ressources

- [React v16](https://github.com/facebook/react/issues/10294)